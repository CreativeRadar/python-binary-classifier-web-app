DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GenerateCorrectLabel`(IN user_input VARCHAR(100),
									IN feedback CHAR(20),
									IN systems_prediction TINYINT(1),
									OUT correct_label TINYINT(1))
BEGIN
	
	IF feedback = "0" and systems_prediction = 0 THEN
		SET correct_label = 1;
	ELSEIF feedback = "0" and systems_prediction= 1 THEN
		SET correct_label = 0;
	END IF;
	INSERT INTO user_feedback(user_entry,systems_prediction,feedback,correct_text_category)
    VALUE(user_input,systems_prediction,feedback,correct_label);
END$$
DELIMITER ;
