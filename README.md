# Python Binary Classifier Web App

A Sentiment classifier that predicts if a piece of data is positive or negative.
It classifies a piece of input text into positive or negative categories and retrains the 
clasisifier model using the inputted text for better vocabulary.
