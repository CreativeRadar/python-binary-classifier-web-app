from flask import Flask, render_template, request, url_for
from datetime import datetime
from pathlib import Path
import pymysql
import numpy as np
import pickle
import datetime
import os
import time



# connect to mysql server
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='Pa55w.rd',
                             db='python',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


app = Flask(__name__)

def save_vectorizer():
	print('Saving Vectoriser Path')
	

	with connection.cursor() as cursor:
		path = 'models/vectorizer/vectorizer.pickle'			
		sql = "INSERT INTO `vectoriser_path` (`vectoriser_path`) VALUES (%s)"
		cursor.execute(sql, path)
		
		#save changes
		connection.commit()


def save_model():

	print('Saving classifier')

	with connection.cursor() as cursor2:

		path = 'models/classifier.pickle'
		sql = "INSERT INTO `model_path` (`model_path`) VALUES (%s)"
		cursor2.execute(sql, path)
		
		#save changes
		connection.commit()



def load_model():
	global clf
	print('Loading classifier')

	with connection.cursor() as cursor3:
             #Load  the most recent model Update
			sql = "SELECT `model_path` FROM `model_path` ORDER BY `entry_date` DESC LIMIT 1;"
			cursor3.execute(sql)


			path1 = cursor3.fetchone()
			path2 = path1.get("model_path", "")
			path = str(path2)
			print(path)

			#save changes
			connection.commit()

	with open(path, 'rb') as fid:
		clf = pickle.load(fid)
		print(clf)
		


def load_vectorizer():
	global vectorizer
	print('Loading vectorizer')

	with connection.cursor() as cursor4:

			sql = "SELECT `vectoriser_path` FROM `vectoriser_path`"
			cursor4.execute(sql)


			path1 = cursor4.fetchone()
			path2 = path1.get("vectoriser_path", "")
			path = str(path2)


			#save changes
			connection.commit()

	with open(path, 'rb') as fid:
		vectorizer = pickle.load(fid)

@app.route('/')
#@app.route('/index')
def form():

	return render_template('index.html')


@app.route('/submitted', methods=['GET', 'POST'])
def submitted_form():
	global pred
	global vec
	text=request.form['input_text']
	print('text:',text)
	vec=vectorizer.transform([text])
	pred=str(clf.predict(vec)[0])
	print('pred:',pred)

	return render_template('predicted_form.html',sentence=text,prediction=pred)


@app.route('/feedback_received', methods=['POST'])
def feedback_received():


    feedback = request.form['feedback']
    sentence = request.form['sentence']
	
 

    try:
        with connection.cursor() as cursor5:
		
			# Insert new record and generate correct label for input text
            args = (sentence,feedback,pred,0)
            cursor5.callproc("GenerateCorrectLabel", args)


            #Retraining the Model with User Entry and correct label(y_train)
            sql2 = "SELECT `correct_text_category` FROM `user_feedback` WHERE `user_entry` = %s ORDER BY entry_date DESC LIMIT 1"
            cursor5.execute(sql2, sentence)
            result_set = cursor5.fetchone()
			
			#Get value of result-set dictionary
            result_set_value = result_set.get("correct_text_category", "")
            
            
			#Change shape of result set
            y = np.ravel(result_set_value)
			
			#Retrain Model
            clf2 = clf.partial_fit(vec,y)
            print("Model Retrained")
			
			#Persist Model: create and save different versions of model with timestamp
            t = time.localtime()
            timestamp = time.strftime('%b-%d-%Y_%H%M', t)
			
            New_model_path = "models/Retrained classifier- On" +timestamp+ ".pickle"
            print(len(New_model_path))
            arg2 = str(New_model_path)
          
            pickle_out = open(arg2,"wb")
            pickle.dump(clf2, pickle_out)
            pickle_out.close()
			
			
			#Save New Model Path into Database
            sql = "INSERT INTO `model_path` (`model_path`) VALUES (%s)"
            cursor5.execute(sql, New_model_path)
		
			
			
        connection.commit()
            


    finally:
        connection.close()
		
        
		

    return render_template('feedback_received.html', sentence=sentence, feedback=feedback)

#Adapted from stack overflow to suit project needs https://stackoverflow.com/questions/12485666/python-deleting-all-files-in-a-folder-older-than-x-days
def clean_modelpath_directory():
    path = r"./models"
    now = time.time()

    for file in os.listdir(path):
        if (file.startswith("R")) and (os.path.getmtime(os.path.join(path, file)) < now - 1 * 86400):
            if os.path.isfile(os.path.join(path, file)):
                os.remove(os.path.join(path, file))




if __name__ == '__main__':
    save_vectorizer()
    load_model()
    load_vectorizer()
    clean_modelpath_directory()

    app.run(host='0.0.0.0', port=8080, debug=True)
