#Delete event

DELIMITER $$
CREATE EVENT deleteOldModelPaths
	ON SCHEDULE 
		EVERY 1 HOUR
	ON COMPLETION PRESERVE
	COMMENT 'Clean Up Old models every hour'
	DO

		BEGIN
    
			DELETE FROM model_path WHERE id != 1;
      
		END $$;
DELIMITER ;