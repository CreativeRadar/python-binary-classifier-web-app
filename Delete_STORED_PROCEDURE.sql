#Initial Delete Stored Procedure ( NO longer USED IN APPLICATION)
DELIMITER //
CREATE PROCEDURE InsertNewModelPath(IN new_model_path VARCHAR(100))
BEGIN
	DECLARE num INT DEFAULT 0;
    START TRANSACTION;
	SELECT count(*) INTO num  FROM model_path;
	IF num  >= 10 THEN
		DELETE FROM model_path WHERE id NOT IN (SELECT * FROM 
					(SELECT id FROM model_path
                    ORDER BY id
						DESC LIMIT 10) m);
	END IF;
    
	 INSERT INTO model_path(model_path) VALUES (new_model_path);
    
    COMMIT;
    
END //
DELIMITER ;